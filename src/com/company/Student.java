package com.company;

public class Student
{
    public int studentNumber;
    public int labsCnt;
    public String subjectName;

    Student(Integer number)
    {
        this.studentNumber = number;
        this.labsCnt = Data.labsСnt[(int) (Math.random() * Data.subjects.length)];
        this.subjectName = Data.subjects[(int) (Math.random() * Data.subjects.length)];
    }

    public void changeLabsCnt(Integer change)
    {
        this.labsCnt = labsCnt - change;
    }

    @Override
    public String toString()
    {
        return ("Студент " + studentNumber + " имеет " + labsCnt + " лаб по предмету " + subjectName);
    }
}
