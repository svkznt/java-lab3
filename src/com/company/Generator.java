package com.company;

public class Generator implements Runnable
{
    private StudentLine studentLine;
    public Generator(StudentLine studentLine)
    {
        this.studentLine = studentLine;
    }

    @Override
    public void run()
    {
        try
        {
            int i = 1;
            while (i <= Data.studentsCount)
            {
                if (studentLine.getQueue().size() != Data.queueLength)
                {
                    Student student = new Student(i);
                    synchronized(studentLine)
                    {
                        studentLine.put(student);
                        System.out.println("Новый студент встал в очередь " + student);
                        studentLine.notifyAll();
                    }
                    ++i;
                }
            }
            studentLine.setStatus(false);
            System.out.println("Все студенты встали в очередь/были обработаны ");
        }
        catch (InterruptedException exc)
        {
            exc.printStackTrace();
        }
    }
}
