package com.company;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class Main
{
    public static void main(String[] args)
    {
        System.out.println("Роботы начали принимать лабы студентов");
        try
        {
            StudentLine studentLine = new StudentLine();
            ExecutorService threads = Executors.newFixedThreadPool(Data.threadsCount);
            threads.execute(new Robot(Data.subjects[0], studentLine));
            threads.execute(new Robot(Data.subjects[1], studentLine));
            threads.execute(new Robot(Data.subjects[2], studentLine));
            threads.submit(new Generator(studentLine));
            threads.shutdown();
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
        }
    }
}
