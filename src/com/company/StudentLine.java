package com.company;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.ArrayBlockingQueue;

public class StudentLine
{
    private ArrayBlockingQueue<Student> studentQueue;
    private Boolean flag;

    public StudentLine()
    {
        studentQueue = new ArrayBlockingQueue<Student>(Data.queueLength);
        flag = true;
    }

    public void put(Student student) throws InterruptedException
    {
        studentQueue.put(student);
    }

    public Student get() throws InterruptedException
    {
        return studentQueue.poll(1, TimeUnit.SECONDS);
    }

    public Student peek() throws InterruptedException
    {
        return studentQueue.peek();
    }

    public ArrayBlockingQueue<Student> getQueue()
    {
        return studentQueue;
    }

    public void setStatus(Boolean newStatus)
    {
        flag = newStatus;
    }

    public Boolean getStatus()
    {
        return flag;
    }
}
