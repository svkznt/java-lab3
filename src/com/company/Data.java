package com.company;

public class Data
{
    public static int queueLength = 10;
    public static int labsAcceptance = 5;
    public static int[] labsСnt = {10, 20, 100};
    public static String[] subjects = {"ООП", "Физика", "Высшая математика"};
    public static int threadsCount = 4;
    public static long sleepTime = 700;
    public static int studentsCount = 21;
}
