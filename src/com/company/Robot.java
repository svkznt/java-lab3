package com.company;

public class Robot implements Runnable
{
    private String subjectName;
    private StudentLine studentLine;

    public Robot(String subject, StudentLine broker)
    {
        this.subjectName = subject;
        this.studentLine = broker;
    }

    @Override
    public void run()
    {
        try
        {
            while (true)
            {
                //System.out.println(subjectName + " работает ");
                Student student = null;
                synchronized (studentLine)
                {
                    if (!studentLine.getStatus() && studentLine.getQueue().size() == 0)
                        break;

                    while(studentLine.getQueue().size() == 0)
                    {
                        studentLine.wait();
                    }

                    if (studentLine.peek() != null)
                    {
                        if (studentLine.peek().subjectName.equals(subjectName))
                        {
                            student = studentLine.get();
                            studentLine.notifyAll();
                        }
                        else
                        {
                            studentLine.wait();
                        }
                    }
                }

                if (student != null)
                {
                    while (student.labsCnt != 0)
                    {
                        System.out.println(subjectName + " робот работает с " + student);
                        student.changeLabsCnt(Data.labsAcceptance);
                        Thread.sleep(Data.sleepTime);
                    }
                    System.out.println(subjectName + " робот завершил работу с " + student.studentNumber);
                }
            }
        }
        catch (InterruptedException exc)
        {
            exc.printStackTrace();
        }
    }
}
