package com.company;

/*import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите количество потоков: ");
        int n = in.nextInt();
        //static PooledExecutor pool = new
        Plug lock = new Plug();
        try
        {
            ExecutorService threads = Executors.newFixedThreadPool(n);
            for (int i = 0; i < n; ++i)
            {
                threads.execute(new Robot(lock, n, i));
            }
            threads.shutdown();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}*/




import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите количество потоков: ");
        int numberOfStrings = in.nextInt();

        QueueOrder[] arrQueue = new QueueOrder[numberOfStrings];
        Thread[] arrThread = new Thread[numberOfStrings];

        for (int i = 0; i < numberOfStrings; ++i)
        {
            arrQueue[i] = new QueueOrder(numberOfStrings, i);
            arrThread[i] = new Thread(arrQueue[i]);
        }

        for (int i = 0; i < numberOfStrings; ++i)
        {
            if (i == (numberOfStrings - 1))
                arrQueue[i].setChild(arrQueue[0]);
            else
                arrQueue[i].setChild(arrQueue[i + 1]);

            if (i == 0)
                arrQueue[i].isPrinted = true;
        }

        for (int i = 0; i < numberOfStrings; ++i)
        {
            arrThread[i].start();
        }
    }
}

class QueueOrder implements Runnable
{
    QueueOrder child;
    boolean isPrinted = false;
    int numberOfStrings;
    int n;

    public QueueOrder(int numberOfStrings, int n)
    {
        this.numberOfStrings = numberOfStrings;
        this.n = n + 1;
    }

    public QueueOrder getChild()
    {
        return child;
    }

    public void setChild(QueueOrder child)
    {
        this.child = child;
    }

    @Override
    public void run()
    {
        try
        {
            for (int i = 0; i < numberOfStrings; ++i)
            {
                synchronized (this)
                {
                    if (!this.isPrinted)
                    {
                        this.wait();
                    }

                    System.out.print(Thread.currentThread().getName() + " ");

                    if (n == numberOfStrings)
                        System.out.println("\n");
                    //Thread.sleep(500);

                    this.isPrinted = false;
                }

                synchronized (this.child)
                {
                    if (!this.child.isPrinted)
                    {
                        this.child.isPrinted = true;
                        this.child.notify();
                    }
                }
            }
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
